package ;

import haxe.ui.containers.VBox;
import haxe.ui.events.KeyboardEvent;
import haxe.ui.core.Component;
import haxe.ui.events.MouseEvent;

@:build(haxe.ui.ComponentBuilder.build("Assets/main-view.xml"))
class MainView extends VBox {
	var entries:Array<ArcadeEntry> = [];
	
	public function new() {
		super();
	}

	@:bind(searchbar, KeyboardEvent.KEY_UP)
	function onSearchUpdate(e){
		if(searchbar.text != null){ //for some reason android appears to be incapable of handling the null itself. you don't need this check when compiling for desktop.
			filterView(new EReg(searchbar.text, "i"));
		}
}

@:bind(sidebarshow, MouseEvent.CLICK)
function showSidebar(e){
	var sidebar = new MainSidebar();
	sidebar.show();
}
	
	function clearView(){
		for(a in entries){
			entrycontainer.removeComponent(a);
		}
		entries = [];
	}

	public function reloadView(arcades:Array<Arcade>){
		clearView();
		
		for(a in arcades){
			var newEntry = new ArcadeEntry(a);
			entries.push(newEntry);
			entrycontainer.addComponent(newEntry);
		}
	}

	function filterView(filterString:EReg){
		for(a in entries){
			a.hide();
			
			if(filterString.match(a.ref.name) || filterString.match(a.ref.location)){
				a.show();
			}
			else{
				for(g in a.ref.games){
					if(filterString.match(g.name)){
						a.show();
					}
				}
			}
		}
	}
}