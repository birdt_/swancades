package ;

import haxe.ui.events.MouseEvent;

import haxe.ui.components.Label;

import haxe.ui.containers.VBox;
import haxe.ui.containers.ScrollView;

@:build(haxe.ui.ComponentBuilder.build("Assets/arcade-entry.xml"))
class ArcadeEntry extends VBox {
	var arcadeName:String;
	var location:String;

	var expanded:Bool = false;
	var expansionAmount:Int;

	public var ref:Arcade;
	
	public function new(arcade:Arcade) {
		super();
		
		arcadeName = arcade.name;
		location = arcade.location;	

		ref = arcade;
		
		switch(arcade.brand){
			case "tz":
			logo.resource = "Assets/Logos/timezone.png";
		}
		
		updateName();
		updateLocation();	

		for(g in arcade.games){
			gamelist.addComponent(new GameEntry(g));
			expansionAmount += 40;
		}

		gamelist.hide();
		
		onClick = function (e:MouseEvent){
			toggleExpanded();
			}

		
	}

	function updateName(){
		arcadename.text = arcadeName;
	}

	function updateLocation(){
		arcadelocation.text = location;
	}

	function toggleExpanded(){
		if(expanded){
			height-=expansionAmount;
			gamelist.hide();
		}
		else{
			height+=expansionAmount;
			gamelist.show();
		}
		expanded = !expanded;
	}
}
