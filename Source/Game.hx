class Game{
	public var name:String;
	public var price:String;
	public var quality:String;
	public var notes:String;
	public var img:String;
	
	public var arcade:Arcade;
	
	public function new(_game:Dynamic, _arcade:Arcade){
		name = _game.name;
		price = _game.price;
		notes = _game.notes;
		img = _game.img;
		arcade = _arcade;

		var qualityString:String = "Not Rated";
		switch(_game.quality){
			case 1:
			qualityString = "Unplayable";
			case 2:
			qualityString = "Bad";
			case 3:
			qualityString = "Playable";
			case 4:
			qualityString = "Good";
			case 5:
			qualityString = "Perfect";
		}
		quality = qualityString;
	}
}