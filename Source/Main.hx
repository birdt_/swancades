package ;

import haxe.ui.HaxeUIApp;
import haxe.ui.core.Component;
import haxe.ui.containers.VBox;

import haxe.ui.Toolkit;

class Main {
	//Location of the arcades.json
	static var arcadeLocation = "https://bitbucket.org/birdt_/swancades/raw/HEAD/arcades.json";
	static var arcades:Array<Arcade> = [];

	static var mainView:MainView;

	public static function main() {	
		var app = new HaxeUIApp();
		Toolkit.init();
		Toolkit.theme = "dark";
		app.ready(function() {
				mainView = new MainView();
				app.addComponent(mainView);

				app.start();

				loadArcades();
				mainView.reloadView(arcades);
			});
	}

	static function loadArcades(){
		arcades = [];
		
		var arcadesHttp = new haxe.Http(arcadeLocation);

		arcadesHttp.onData = function(data:String){
			try{
				var arcadesJson = haxe.Json.parse(data);

				var a:Array<Dynamic> = arcadesJson.arcades;

				for(i in a){
					arcades.push(new Arcade(i));
				}
			}
			catch(e){
				trace(e);
			}
		}

		arcadesHttp.onError = function(error){
			trace("Invalid URL: " + error);
		}

		arcadesHttp.request();
	}

	public static function swapJson(newLocation:String){
		arcadeLocation = newLocation;

		loadArcades();

		mainView.reloadView(arcades);
	}

	public static function getJson():String{
		return arcadeLocation;
	}
}
